let gridSize = 16;
let canvasSize = 680 + "px";

let container = document.querySelector("#container");
container.style.display = "flex";
container.style.width = canvasSize;
container.style.height = canvasSize;
container.style.flexWrap = "wrap";
container.style.border = "1px solid black";

function createGrid() {
  for (let i = 0; i < gridSize * gridSize; i += 1) {
    let square = document.createElement("div");
    square.className = "square";
    container.appendChild(square);
    document.getElementsByClassName("square")[i].style.height = canvasSize / gridSize - 2;
    document.getElementsByClassName("square")[i].style.border = "1px solid black";
    document.getElementsByClassName("square")[i].style.flexBasis = "calc(" + 100 / gridSize + "% - 2px)";
    document.getElementsByClassName("square")[i].addEventListener("mouseover", mouseOver);
  }
}

function destroyGrid() {
  while (container.hasChildNodes()) {
    container.removeChild(container.lastChild);
  }
}

function mouseOver() {
  this.style.backgroundColor = "black";
}

function resizeGrid() {
  gridSize = prompt("What size would you like the grid?");
  destroyGrid();
  createGrid();
}

function clearGrid() {
  for (let i = 0; i < gridSize * gridSize; i += 1) {
    document.getElementsByClassName("square")[i].style.backgroundColor = "white";
  }
}

createGrid();

document.getElementById("reset").onclick = function() {
  clearGrid()
};
document.getElementById("change").onclick = function() {
  resizeGrid()
};
